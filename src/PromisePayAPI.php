<?php

namespace Drupal\commerce_promisepay;

use Drupal\commerce_payment\Entity\PaymentGateway;
use PromisePay\PromisePay;
use PromisePay\Exception\Api as ApiException;
use PromisePay\Exception\Unauthorized;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Session\AccountInterface;

class PromisePayAPI {

  /**
  * Loads the first active promisepay gateway from storage and initialises from config
  */
  private static function bootstrapPlugin() {
    $gateways = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway')->loadMultiple();
    /** @var PaymentGateway $gateway */
    foreach ($gateways as $gateway) {
      if ($gateway->get('plugin') == 'promisepay_onsite' && $gateway->get('status') == TRUE) {
        if (defined('promisepay\API_LOGIN')) return;
        $plugin = $gateway->getPlugin();
        if ($plugin->getMode() == 'test') {
          PromisePay::Configuration()->environment('prelive');
        } else {
          PromisePay::Configuration()->environment('production');
        }
        $config = $plugin->getConfig();
        PromisePay::Configuration()->login($config['api_email']);
        PromisePay::Configuration()->password($config['api_token']);
        return;
      }
    }
    throw new PaymentGatewayException('PromisePay envionment not set! add a PromisePay payment gateway in Commerce Payment Gateways');
  }

  /**
   * Finds out if promisepay is the test environment
   *
   * @return boolean true if prelive
   */
  public static function isTestEnvironment() {
    $gateways = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway')->loadMultiple();
    /** @var PaymentGateway $gateway */
    foreach ($gateways as $gateway) {
      if ($gateway->get('plugin') == 'promisepay_onsite' && $gateway->get('status') == TRUE) {
        $plugin = $gateway->getPlugin();
        if ($plugin->getMode() == 'test') {
          return true;
        } else {
          return false;
        }
      }
    }
    throw new PaymentGatewayException('PromisePay envionment not set! add a PromisePay payment gateway in Commerce Payment Gateways');
  }

  /**
   * Get an item state string value
   *
   * @param string $code item state code
   * @return string|null item state
   */
  public static function itemState($code) {
    $hash = [
      '22000' => 'pending',
      '22100' => 'payment_required',
      '22110' => 'wire_pending',
      '22111' => 'paypal_pending',
      '22150' => 'payment_pending',
      '22175' => 'payment_held',
      '22180' => 'payment_authorized',
      '22195' => 'voided',
      '22190' => 'fraud_hold',
      '22200' => 'payment_deposited',
      '22300' => 'work_completed',
      '22400' => 'problem_flagged',
      '22410' => 'problem_resolve_requested',
      '22420' => 'problem_escalated',
      '22500' => 'completed',
      '22575' => 'cancelled',
      '22600' => 'refunded',
      '22610' => 'refund_pending',
      '22650' => 'refund_flagged',
      '22670' => 'off_platform_refunded',
      '22700' => 'partial_completed',
      '22800' => 'partial_paid',
      '22680' => 'off_platform_chargedback',
    ];
    if (isset($hash[$code])){
      return $hash[$code];
    } 
    return null;
  }


  public static function marketplace() {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::Marketplaces()->get();
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  public static function listConfigurations($params = []) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::Configurations()->getList($params);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

/**
  * creates a user on promisepay for the platform
  *
  * @param \Drupal\Core\Session\AccountInterface $user
  *
  * @return array the response
  */
  public static function createUserFromUser(AccountInterface $user) {
    $user_values = [
      'uuid'        => $user->uuid(),
      'email'       => $user->getEmail(),
      'first_name'  => $user->getAccountName()
    ];
    return self::createUser($user_values);
  }

  /**
  * creates a user on promisepay for the platform
  *
  * @param array $user_values
  *   can have the following valid keys:
  *    "id" => "5830def0-ffe8-11e5-86aa-5e5517507c66",  << required, unique for platform
  *    "email" => "samuel.seller@promisepay.com",       << required, unique for platform
  *    "first_name" => "Samuel",                        << required
  *    "last_name" => "Seller",
  *    "mobile" => "+61491570156",
  *    "address_line1" => "100 Main Street",
  *    "address_line2" => "",
  *    "state" => "NSW",
  *    "city" => "Sydney",
  *    "zip" => "2000",
  *    "country" => "AUS",
  *    "dob" => "15/06/1980",
  *    "government_number" => "10203040",
  *    "drivers_license_number" => "",
  *    "drivers_license_state" => "",
  *    "ip_address" => "",
  *
  * @return array the response
  */
  public static function createUser($user_values) {
    $params = [];
    if (!empty($user_values['uuid'])) {
      $params['id'] = $user_values['uuid'];
    } else {
      throw new PaymentGatewayException('PromisePay user cannot be created without a UUID');
    }
    if (!empty($user_values['email'])) {
      $params['email'] = $user_values['email'];
    } else {
      throw new PaymentGatewayException('PromisePay user cannot be created without an email');
    }
    if (!empty($user_values['first_name'])) {
      $params['first_name'] = $user_values['first_name'];
    } else {
      throw new PaymentGatewayException('PromisePay user cannot be created without a username');
    }
    $params['last_name'] = isset($user_values['last_name']) ? $user_values['last_name'] : '';
    $params['mobile'] = isset($user_values['mobile']) ? $user_values['mobile'] : '';
    $params['address_line1'] = isset($user_values['address_line1']) ? $user_values['address_line1'] : '';
    $params['address_line2'] = isset($user_values['address_line2']) ? $user_values['address_line2'] : '';
    $params['state'] = isset($user_values['state']) ? $user_values['state'] : '';
    $params['city'] = isset($user_values['city']) ? $user_values['city'] : '';
    $params['zip'] = isset($user_values['zip']) ? $user_values['zip'] : '';
    $params['country'] = isset($user_values['country']) ? $user_values['country'] : 'US';
    $params['dob'] = isset($user_values['dob']) ? $user_values['dob'] : '';
    $params['government_number'] = isset($user_values['government_number']) ? $user_values['government_number'] : '';
    $params['drivers_license_number'] = isset($user_values['drivers_license_num)ber']) ? $user_values['drivers_license_number'] : '';
    $params['drivers_license_state'] = isset($user_values['drivers_license_state']) ? $user_values['drivers_license_state'] : '';
    $params['ip_address'] = isset($user_values['ip_address']) ? $user_values['ip_address'] : '';

    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::User()->create($params);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $dup_uuid = false;
        $dup_email = false;
        if (stripos($message, 'principal.user_auths.external_id')) {
          // User ID has already been taken; no need to create
          $dup_uuid = true;
        }
        if (stripos($message, 'principal.email')) {
          // email has already been taken
          $dup_email = true;
        }

        if ($dup_uuid) {
          $response = ['error' => 'user_id_exists'];
        } else if (!$dup_uuid && $dup_email) {
          $response = ['error' => 'user_email_exists'];
          // get the user with this email
          $userlist = self::listUsers(2,0,$user_values['email']);
          if (count($userlist) == 1) {
            // and return the user as part of the error response
            $response['promisepay_user'] = $userlist[0];
          } else {
          drupal_set_message($user_values['email'] . ' is already in use by a different user.', 'error');
        }
        } else {
          throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
        }

      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }

    return $response;
  }


  /**
  * updates a user on promisepay for the platform
  *
  * @param string $id
  *   uuid of the user
  * @param array $user_values
  *   can have the following valid keys:
  *    "email" => "samuel.seller@promisepay.com",
  *    "first_name" => "Samuel",
  *    "last_name" => "Seller",
  *    "mobile" => "+61491570156",
  *    "address_line1" => "100 Main Street",
  *    "address_line2" => "",
  *    "state" => "NSW",
  *    "city" => "Sydney",
  *    "zip" => "2000",
  *    "country" => "AUS",
  *    "dob" => "15/06/1980",
  *    "government_number" => "10203040",
  *    "drivers_license_number" => "",
  *    "drivers_license_state" => "",
  *    "ip_address" => "",
  * @param boolean $async
  *
  * @return array the response
  */
  public static function updateUser($id, $user_values, $async = false) {
    if (empty($id)) {
      throw new PaymentGatewayException('PromisePay user id is required');
    }

    PromisePayAPI::bootstrapPlugin();

    $response = null;
    if ($async) {
      $update = function() use ($id, $user_values) {
        PromisePay::User()->update($id, $user_values);
      };
      PromisePay::AsyncClient($update)->done($user_response);
      if (isset($user_response['error'])) {
        $error = $user_response['error'];
        \Drupal::logger('promisepay_api')->error($error);
      }
    } else {
      try {
        $response = PromisePay::User()->update($id, $user_values);
      } catch (ApiException $e) {
        $code = $e->getCode();
        if ($code == 422 || $code == 0) {
          $message = $e->getMessage();
          $response = ['error' => $message];
        }
        else {
          throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
        }
      } catch (Unauthorized $e) {
        $code = $e->getCode();
        if ($code == 401 || $code == 0) {
          $message = $e->getMessage();
          $response = ['error' => $message];
        }
        else {
          throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
        }
      }
    }

    return $response;
  }

  /**
  * lists the platform users
  *
  * @param int $limit
  *
  * @param int $offset
  *
  * @param string $search
  *   Search fields of the user for the string, limits the result
  *
  * @return array of users (assoc array)
  */
  public static function listUsers($limit = 200, $offset = 0, $search = '') {
    PromisePayAPI::bootstrapPlugin();
    $params = [
      'limit'  => $limit,
      'offset' => $offset,
      'search' => $search
    ];
    try {
      $response = PromisePay::User()->getList($params);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * get user info
   *
   * @param string $uuid
   *
   * @return array of user data (assoc array)
   */
  public static function showUser($uuid) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::User()->get($uuid);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }


  /**
   * get user bank account
   *
   * @param string $uuid
   *
   * @return array of user data (assoc array)
   */
  public static function showUserBankAccount($uuid) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::User()->getListOfBankAccounts($uuid);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }


  /**
   * get user wallet account
   *
   * @param string $uuid
   *
   * @return array of user data (assoc array)
   */
  public static function showUserWalletAccount($uuid) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::User()->getListOfWalletAccounts($uuid);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }


  /**
   * get user bank account
   *
   * @param array $values
   *  "user_id"        => 'eff58a2d-074f-443c-949f-99de1008ad86',
   *  "bank_name"      => 'Citizens Bank',
   *  "account_name"   => 'Ransom J Weaver',
   *  "routing_number" => '036076150',
   *  "account_number" => 'XXXXXXXXXXX',
   *  "account_type"   => 'checking',
   *  "holder_type"    => 'personal',
   *  "country"        => 'USA',
   *
   *
   * @return array of response data (assoc array)
   */
  public static function createBankAccount($values) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::BankAccount()->create($values);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * Redacts a bank account
   *
   *
   * @param string $account_id the uuid of the bank account
   *
   * @return array of response data (assoc array) or array with 'error' key
   */
  public static function redactBankAccount($account_id) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::BankAccount()->delete($account_id);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) { // webservice bug? this has status code 0 but says 422 in the messsage
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * sets the user's disbursement account
   *
   * @param string $uuid
   *
   * @param string $account_id the uuid of the bank or paypal account
   *
   * @return array of user data (assoc array) or array with 'error' key
   */
  public static function setDisbursementAccount($uuid, $account_id) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::User()->setDisbursementAccount($uuid, ['account_id' => $account_id]);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) { // webservice bug? this has status code 0 but says 422 in the messsage
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }


  /**
   * creates a company on promisepay for the platform
   *
   * @param array $company_values
   *   can have the following valid keys:
   *    "name" => "My Company",
   *    "legal_name" => "My Company Inc",
   *    "user_id" => "",
   *    "phone" => "+61491570156",
   *    "tax_number" => "23-19283292",
   *    "charge_tax" => 'false',
   *    "address_line1" => "100 Main Street",
   *    "address_line2" => "",
   *    "state" => "NSW",
   *    "city" => "Sydney",
   *    "zip" => "2000",
   *    "country" => "AUS",
   * @param boolean $async
   *
   * @return array the response
   */
  public static function createCompany($company_values, $async = false) {
    PromisePayAPI::bootstrapPlugin();

    $response = null;
    if ($async) {
      $create = function() use ($company_values) {
        PromisePay::Company()->create($company_values);
      };
      PromisePay::AsyncClient($create)->done($company_response);
      if (isset($company_response['error'])) {
        $error = $company_response['error'];
        \Drupal::logger('promisepay_api')->error($error);
      }
    } else {
      try {
        $response = PromisePay::Company()->create($company_values);
      } catch (ApiException $e) {
        $code = $e->getCode();
        if ($code == 422 || $code == 0) {
          $message = $e->getMessage();
          $response = ['error' => $message];
        } else {
          throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
        }
      } catch (Unauthorized $e) {
        $code = $e->getCode();
        if ($code == 401 || $code == 0) {
          $message = $e->getMessage();
          $response = ['error' => $message];
        } else {
          throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
        }
      }
    }
    return $response;

  }

  /**
   * updates a company on promisepay for the platform
   *
   * @param string $id
   *   uuid of the company
   * @param array $company_values
   *   can have the following valid keys:
   *    "name" => "My Company",
   *    "legal_name" => "My Company Inc",
   *    "user_id" => "",
   *    "phone" => "+61491570156",
   *    "tax_number" => "23-19283292",
   *    "charge_tax" => 'false',
   *    "address_line1" => "100 Main Street",
   *    "address_line2" => "",
   *    "state" => "NSW",
   *    "city" => "Sydney",
   *    "zip" => "2000",
   *    "country" => "AUS",
   * @param boolean $async
   *
   * @return array the response
   */
  public static function updateCompany($id, $company_values, $async = false) {
    if (empty($id)) {
      throw new PaymentGatewayException('PromisePay company id is required');
    }
    $response = null;
    PromisePayAPI::bootstrapPlugin();
    if ($async) {
      $update = function() use ($id, $company_values) {
        PromisePay::Company()->update($id, $company_values);
      };
      PromisePay::AsyncClient($update)->done($company_response);
      if (isset($company_response['error'])) {
        $error = $company_response['error'];
        \Drupal::logger('promisepay_api')->error($error);
      }
    } else {
      try {
        $response = PromisePay::Company()->update($id, $company_values);
      } catch (ApiException $e) {
        $code = $e->getCode();
        if ($code == 422 || $code == 0) {
          $message = $e->getMessage();
          $response = ['error' => $message];
        } else {
          throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
        }
      }
    }

    return $response;
  }

  /**
   * get company info
   *
   * @param string $uuid
   *
   * @return array of company data (assoc array)
   */
  public static function showCompany($uuid) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::Company()->get($uuid);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * get token for card and bank creation via form
   *
   * @param string $user_uuid
   *
   * @param boolean $is_bank
   *
   * @return array with 'error' or 'token'
   */
  public static function generateCardToken($user_uuid, $is_bank = false) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::Token()->generateCardToken([
        'token_type' => ($is_bank ? 'bank' : 'card'),
        'user_id' => $user_uuid
      ]);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * Create an item on PromisePay
   *
   * @param $values
   *    "id" => '100fd4a0-0538-11e6-b512-3e1d05defe78',
   *    "name" => 'Landscaping Job #001',
   *    "amount" => 2000,
   *    "payment_type_id" => 2, << 1 = Escrow, 2 = Express, 3 = Escrow Partial Release, 4 = Approve
   *    "buyer_id" => '064d6800-fff3-11e5-86aa-5e5517507c66',
   *    "seller_id" => '5830def0-ffe8-11e5-86aa-5e5517507c66',
   *    "fee_ids" => '36020976-f345-4d0f-b860-9c025ccce668',
   *    "description" => 'Planting of natives, removal of tree stump.',
   *    "due_date" => '22/04/2016',
   *    "custom_descriptor" => 'When custom_descriptors are enabled, appears on credit card statement.',
   *
   * @return array response data
   */
  public static function createItem($values) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::Item()->create($values);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * Create an item on PromisePay
   *
   * @param string $uuid
   *    the uuid of the payment being created
   *
   * @return array response data
   */
  public static function deleteItem($uuid) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::Item()->delete($uuid);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * Create a Fee on PromisePay
   *
   * @param $values
   *    "name" => 'Seller success fee',
   *    "amount" => 2000,
   *    "fee_type_id" => 1, << 1 = Fixed,  2 = Percentage, 3 = Percentage with Cap, 4 = Percentage with Min
   *    "cap => '',
   *    "min" => '',
   *    "max" => '',
   *    "to" => 'seller', << who pays the fee (buyer, seller, cc, int_wire)
   *
   * @return array response data
   */
  public static function createFee($values) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::Fee()->create($values);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * Make payment on a PromisePay Item
   *
   * @param string $item_id
   * @param string $payment_id
   * @param string $ip
   * @param string $device_id
   *
   * @return array response data
   */
  public static function makePayment($item_id, $payment_id, $ip = NULL, $device_id = NULL) {
    PromisePayAPI::bootstrapPlugin();
    $values = [
      'account_id' => $payment_id,
    ];
    if ($ip) $values['ip_address'] = $ip;
    if ($device_id) $values['device_id']  = $device_id;
    try {
      $response = PromisePay::Item()->makePayment($item_id, $values);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * Release the payment on a PromisePay Item
   *
   * @param $item_id
   * @return array response data
   */
  public static function releasePayment($item_id) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::Item()->releasePayment($item_id);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * Refund the payment on a PromisePay Item
   *
   * @param $item_id
   * @param int $amount
   * @param string $message
   * @return array response data
   */
  public static function refund($item_id, $amount = null, $message = null) {
    PromisePayAPI::bootstrapPlugin();
    $values = [];
    if ($amount) $values['refund_amount'] = $amount;
    if ($message) $values['refund_message'] = $message;

    try {
      $response = PromisePay::Item()->refund($item_id, $values);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * Cancel a PromisePay Item
   *
   * @param $item_id
   * @return array response data
   */
  public static function cancelItem($item_id) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::Item()->cancelItem($item_id);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * Get card account info
   *
   * @param string $uuid
   * @return array response data
   */
  public static function showCardAccount($uuid) {
    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::CardAccount()->get($uuid);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * Create the callback on Assembly
   *
   * @param string $description
   * @param string $url
   * @param string $object_type
   * @param string $enabled
   * @return array response data
   */
  public static function createCallback($description, $url, $object_type, $enabled = 'true') {
    $params = [
      'description' => $description,
      'url' => $url,
      'object_type' => $object_type,
      'enabled' => $enabled
    ];

    PromisePayAPI::bootstrapPlugin();
    try {
      PromisePay::RestClient('post', 'callbacks/', $params);
      $response = PromisePay::getDecodedResponse('callbacks');
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * list the callbacks on Assembly for the platform
   *
   * @param integer $limit
   * @param integer $offset
   * @param string $filter
   * @param string $id
   * @return array response data
   */
  public static function listCallbacks($limit = 200, $offset = 0, $filter = '', $id = '') {
    $params = [
      'limit' => $limit,
      'offset' => $offset,
      'filter' => $filter,
      'id' => $id
    ];

    PromisePayAPI::bootstrapPlugin();
    try {
      PromisePay::RestClient('get', 'callbacks/', $params);
      $response = PromisePay::getDecodedResponse('callbacks');
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

  /**
   * list the responses to a callback
   *
   * @param string $callbackId
   * @return array response data
   */
  public static function getListResponses($callbackId) {

    PromisePayAPI::bootstrapPlugin();
    try {
      $response = PromisePay::Callbacks()->getListResponses($callbackId);
    } catch (ApiException $e) {
      $code = $e->getCode();
      if ($code == 422 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    } catch (Unauthorized $e) {
      $code = $e->getCode();
      if ($code == 401 || $code == 0) {
        $message = $e->getMessage();
        $response = ['error' => $message];
      } else {
        throw new PaymentGatewayException("PromisePay API Failure: " . $e->getMessage());
      }
    }
    return $response;
  }

}
