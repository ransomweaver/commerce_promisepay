<?php

namespace Drupal\commerce_promisepay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\commerce_promisepay\PromisePayAPI;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Provides the Onsite payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "promisepay_onsite",
 *   label = "PromisePay (Onsite)",
 *   payment_type = "payment_promisepay",
 *   display_label = "PromisePay",
 *    forms = {
 *     "add-payment-method" = "Drupal\commerce_promisepay\PluginForm\Onsite\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "mastercard", "visa", "discover",
 *   },
 * )
 */
class Onsite extends OnsitePaymentGatewayBase implements OnsiteInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

  }

  /**
  * get config to elsewhere initialize promisepay from the plugin config values
  *
  * @return array the configuration
  */
  public function getConfig() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_token' => '',
      'api_email' => '',
      'api_user_uuid' => '',
      'api_wallet_uuid' => '',
      'gateway_percent' => 0,
      'gateway_transaction_fee' => 0,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getJsLibrary() {
    if ($this->getMode() == 'test') {
      return 'commerce_promisepay/promisepay_prelive';
    }
    else {
      return 'commerce_promisepay/promisepay_live';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
      '#default_value' => $this->configuration['api_token'],
      '#required' => TRUE,
    ];

    $form['api_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Email'),
      '#default_value' => $this->configuration['api_email'],
      '#required' => TRUE,
    ];

    $form['api_user_uuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Platform User ID'),
      '#default_value' => $this->configuration['api_user_uuid'],
      '#required' => FALSE,
    ];

    $form['api_wallet_uuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Platform Wallet ID'),
      '#default_value' => $this->configuration['api_wallet_uuid'],
      '#required' => FALSE,
    ];

    $form['gateway_percent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Gateway Percent'),
      '#default_value' => $this->configuration['gateway_percent'],
      '#required' => False,
    ];

    $form['gateway_transaction_fee'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Gateway Transaction Fee'),
      '#default_value' => $this->configuration['gateway_transaction_fee'],
      '#required' => False,
    ];

    $form['depreciated'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Depreciated'),
      '#default_value' => $this->configuration['depreciated'],
      '#required' => False,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_token'] = $values['api_token'];
      $this->configuration['api_email'] = $values['api_email'];
      $this->configuration['api_user_uuid'] = $values['api_user_uuid'];
      $this->configuration['api_wallet_uuid'] = $values['api_wallet_uuid'];
      $this->configuration['gateway_percent'] = floatval($values['gateway_percent']);
      $this->configuration['gateway_transaction_fee'] = intval($values['gateway_transaction_fee']);
      $this->configuration['depreciated'] = boolval($values['depreciated']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generateCardToken($order = null) {
    if ($order) {
      $user_uuid = $order->getOwner()->uuid();
    } else {
      $id = \Drupal::currentUser()->id();
      $user = User::load($id);
      $user_uuid = $user->uuid();
    }
    $token = PromisePayAPI::generateCardToken($user_uuid);
    if (!isset($token['error'])) {
      return $token['token'];
    } else {
      drupal_set_message($token['error'], 'error');
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getGatewayPercentage() {
    return $this->getConfig()['gateway_percent'];
  }

  /**
   * {@inheritdoc}
   */
  public function getGatewayTransactionFee() {
    return $this->getConfig()['gateway_transaction_fee'];
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    if ($payment->getState()->value != 'new') {
      throw new \InvalidArgumentException('The provided payment is in an invalid state.');
    }
    $payment_method = $payment->getPaymentMethod();
    if (empty($payment_method)) {
      throw new \InvalidArgumentException('The provided payment has no payment method referenced.');
    }
    if (REQUEST_TIME >= $payment_method->getExpiresTime()) {
      throw new HardDeclineException('The provided payment method has expired');
    }

    // the workflow for a payment is highly platform dependent, so hand off the payment to an integration module via a hook
      \Drupal::moduleHandler()->invokeAll('promisepay_process_payment', [&$payment, $payment_method, $capture, $this]);
  }
  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    if ($payment->getState()->value != 'payment_authorized') {
      throw new \InvalidArgumentException('Only payments in the payment_authorized state can be captured.');
    }
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    $payment_method = $payment->getPaymentMethod();
    if (empty($payment_method)) {
      throw new \InvalidArgumentException('The provided payment has no payment method referenced.');
    }
    if (REQUEST_TIME >= $payment_method->getExpiresTime()) {
      throw new HardDeclineException('The provided payment method has expired');
    }
    // not implemented for this module
    $payment->setAmount($amount);
    $payment->state = 'payment_required';

    // the workflow for a payment is highly platform dependent, so hand off the payment to an integration module via a hook
    \Drupal::moduleHandler()->invokeAll('promisepay_process_payment', [&$payment, $payment_method, TRUE, $this]);
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    if ($payment->getState()->value != 'payment_authorized') {
      throw new \InvalidArgumentException('Only payments in the payment_authorized state can be voided.');
    }

    // Perform the void request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();

    $payment->state = 'voided';
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    if (!in_array($payment->getState()->value, ['payment_deposited, partial_paid'])) {
      throw new \InvalidArgumentException('Only payments in the payment_deposited and partial_paid states can be refunded.');
    }
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    // Validate the requested amount.
    $balance = $payment->getBalance();
    if ($amount->greaterThan($balance)) {
      throw new InvalidRequestException(sprintf("Can't refund more than %s.", $balance->__toString()));
    }

    // Perform the refund request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $amount_cents = intval(floatval($amount->getNumber()) * 100);

    // the workflow for a payment is highly platform dependent, so hand off the refund to an integration module via a hook
    $refund_success = \Drupal::moduleHandler()->invokeAll('promisepay_process_refund', [&$payment, $remote_id,  $amount_cents]);

    if (!$refund_success) {
      drupal_set_message(t('The refund could not be processed'), 'error');
      return;
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->state = 'partial_paid';
    }
    else {
      $payment->state = 'refunded';
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      'card_type', 'last_4', 'expiry_month', 'expiry_year', 'pp_account_id',
    ];

    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        print_r($payment_details);
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    // promisepay payment methods are reusable, but we can't store more than one
    // so we need to update any old one to reusable = false

    $user = $payment_method->getOwner();

    $method_ids = \Drupal::entityQuery('commerce_payment_method')
      ->condition('uid', $user->id())
      ->condition('payment_gateway', 'promisepay')
      ->condition('reusable', true)
      ->execute();
    if (!empty($method_ids)) {
      $existing_pp_payment_methods = PaymentMethod::loadMultiple($method_ids);
      /** @var PaymentMethod $method */
      foreach ($existing_pp_payment_methods as $method) {
        $method->setReusable(false);
        $method->save();
      }
    }

    // Perform the create request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // You might need to do different API requests based on whether the
    // payment method is reusable: $payment_method->isReusable().
    // Non-reusable payment methods usually have an expiration timestamp.
    $payment_method->card_type = $payment_details['card_type'];
    // Only the last 4 numbers are safe to store.
    $payment_method->card_number = $payment_details['last_4'];
    $payment_method->card_exp_month = $payment_details['expiry_month'];
    $payment_method->card_exp_year = $payment_details['expiry_year'];
    $payment_method->ip_address = $payment_details['ip_address'];
    $payment_method->device_id = $payment_details['device_id'];
    $expires = CreditCard::calculateExpirationTimestamp($payment_details['expiry_month'], $payment_details['expiry_year']);
    // The remote ID returned by the request.
    $remote_id = $payment_details['pp_account_id'];

    $payment_method->setRemoteId($remote_id);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();

    $address = $payment_method->getBillingProfile()->address->first();
    $country_code = $address->getCountryCode();

    $mobile = $payment_details['mobile'];
    $mobile = preg_replace("/[^0-9+]/", "", $mobile);
    if (!empty($mobile)) {
      if (strpos($mobile, '1') === 0 || strpos($mobile, '62') === 0 || strpos($mobile, '64') === 0) {
        $mobile = '+' . $mobile;
      }
      else if (strpos($mobile, '+1') !== 0 && strpos($mobile, '+62') !== 0 && strpos($mobile, '+64') !== 0) {
        $mobile = $this->mapCountryCode($country_code) . $mobile;
      }
    }

    $pp_user_values = [
      'email' => $user->getEmail(),
      'first_name' => $address->getGivenName(),
      'last_name' => $address->getFamilyName(),
      'mobile' => $mobile,
      'address_line1' => $address->getAddressLine1(),
      'address_line2' => $address->getAddressLine2(),
      'city' => $address->getLocality(),
      'state' => $address->getAdministrativeArea(),
      'zip' => $address->getPostalCode(),
      'country' => $country_code,
      'ip_address' => $payment_details['ip_address'],
    ];
    PromisePayAPI::updateUser($user->uuid(), $pp_user_values, true);
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * Maps address to phone country code.
   *
   * @param string $country_code
   *   The address country code.
   *
   * @return string
   *   The Phone country code prefixed by +.
   */
  protected function mapCountryCode($country_code) {
    $map = [
      'US' => '+1',
      'AU' => '+61',
      'NZ' => '+64',
    ];

    return $map[$country_code];
  }

}
