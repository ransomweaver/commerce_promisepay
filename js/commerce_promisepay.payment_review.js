/**
 * @file
 * Defines behaviors for the PromisePay payment review page
 */
(function ($, Drupal) {

  'use strict';

  /**
   * Attaches the promisepay review page behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   */
  Drupal.behaviors.commercePromisePayPaymentReview = {
    attach: function (context) {
      $('[id^="commerce-checkout-flow-"]', context).once('promisepay-review').each( function () {
        $( this ).on('submit', Drupal.reviewSubmit);
      });
    },
    detach: function (context) {
      $('[id^="commerce-checkout-flow-"]', context).off('submit');
    }
  };

  Drupal.reviewSubmit = function() {
    var $cover = $( "<div id='cover'></div>" );
    var $body = $('body');
    $body.append($cover);
    var $spinner = $( "<div id='spinner'></div>" );
    $body.append($spinner);
  };

})(jQuery, Drupal);

