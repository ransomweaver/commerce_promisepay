<?php

namespace Drupal\example_promisepay_integration\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Payout portion entity.
 *
 * @ingroup example_promisepay_integration
 *
 * @ContentEntityType(
 *   id = "payout_portion",
 *   label = @Translation("Payout portion"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\example_promisepay_integration\PPPayoutPortionListBuilder",
 *     "views_data" = "Drupal\example_promisepay_integration\Entity\PPPayoutPortionViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\example_promisepay_integration\Form\PPPayoutPortionForm",
 *       "add" = "Drupal\example_promisepay_integration\Form\PPPayoutPortionForm",
 *       "edit" = "Drupal\example_promisepay_integration\Form\PPPayoutPortionForm",
 *       "delete" = "Drupal\example_promisepay_integration\Form\PPPayoutPortionDeleteForm",
 *     },
 *     "access" = "Drupal\example_promisepay_integration\PPPayoutPortionAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\example_promisepay_integration\PPPayoutPortionHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "payout_portion",
 *   admin_permission = "administer payout portion entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "remote_item_id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/payout_portion/{payout_portion}",
 *     "add-form" = "/admin/structure/payout_portion/add",
 *     "edit-form" = "/admin/structure/payout_portion/{payout_portion}/edit",
 *     "delete-form" = "/admin/structure/payout_portion/{payout_portion}/delete",
 *     "collection" = "/admin/structure/payout_portion",
 *   },
 *   field_ui_base_route = "payout_portion.settings"
 * )
 */
class PPPayoutPortion extends ContentEntityBase implements PPPayoutPortionInterface {

  use EntityChangedTrait;

  public function getOrder() {
    return $this->get('order_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrder($order) {
    $this->set('order_id', $order->id());
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmount() {
    return $this->get('amount')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAmount($amount) {
    $this->set('amount', $amount);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteItemId() {
    return $this->get('remote_item_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteItemId($item_id) {
    $this->set('remote_item_id', $item_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPaid() {
    return (bool) $this->getEntityKey('status');
  }
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPaid($paid) {
    $this->set('status', $paid ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Payee'))
      ->setDescription(t('The payee (recipient) of the payment portion'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['remote_item_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remote Item ID'))
      ->setDescription(t('The name of the PromisePay Item used to pay the payee'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order'))
      ->setSetting('target_type', 'commerce_order')
      ->setSetting('handler', 'default');


    $fields['amount'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Amount'))
      ->setDescription(t('The amount of the portion, in cents'))
      ->setSetting('display_description', TRUE)
      ->setDefaultValue(0)
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => 0,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Payout status'))
      ->setDescription(t('A boolean indicating whether the Payout portion is paid out.'))
      ->setDefaultValue(FALSE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
