/**
 * @file
 * Defines behaviors for the PromisePay payment method form.
 */
(function ($, Drupal, drupalSettings, promisepay) {

  'use strict';

  /**
   * Attaches the promisepay behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   */
  Drupal.behaviors.commercePromisePayBankForm = {
    attach: function (context) {
      var bank_form = $('#promisepay-bank-form', context);
      if (bank_form.length > 0) {
        $('#edit-account-number').payment('restrictNumeric');
        $('#edit-routing-number').payment('restrictNumeric');
        promisepay.configure(drupalSettings.promisepay.environment);
        promisepay.captureDeviceId(Drupal.deviceIdSuccess);
        promisepay.getIPAddress(Drupal.ipSuccess);
        promisepay.createBankAccountForm("promisepay-bank-form", Drupal.bankSuccess, Drupal.ppFail);
      }
    },
    detach: function (context) {
      var bank_form = $('#promisepay-bank-form', context);
      if (bank_form.length > 0) {
      }
    }
  };

  Drupal.deviceIdSuccess = function(data) {
    $('#edit-device-id').val(data);
  };

  Drupal.ipSuccess = function(data) {
    $('#edit-ip-address').val(data);
  };

  Drupal.bankSuccess = function(data) {
    var id = data['id'];
    $('#edit-pp-account-id').val(id);
    $('#edit-hidden-holder-type').val($('#edit-holder-type').val());
    $('#edit-bank-name').val('');
    $('#edit-account-name').val('');
    $('#edit-routing-number').val('');
    $('#edit-account-number').val('');
    $('#edit-account-type').val('');
  };

  Drupal.ppFail = function(data) {
    if (typeof(data) === 'string') {
      var errors = $.parseJSON(data);
      var error_string = '<strong>Errors:</strong><br>';
      for (var i = 0, len = errors.length; i < len; i++) {
        var err = errors[i];
        error_string += err['string'] + '<br>';
      }
      $('.promisepay-server-error').html(error_string);
    } else {
      var status = data.statusText;
      if (status) {
        var response = data.responseText.replace('"', '').replace('{', '').replace('}', '').replace('[', ' ').replace(']', ' ');
        $('.promisepay-server-error').html(status + ': ' + response);
      }
    }
  };


})(jQuery, Drupal, drupalSettings, window.promisepay);

