<?php

namespace Drupal\commerce_promisepay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsUpdatingStoredPaymentMethodsInterface;

/**
 * Provides the interface for the promisepay_onsite payment gateway.
 *
 * Onsite payment gateways allow the customer to enter credit card details
 * directly on the site. The details might be safely tokenized before they
 * reach the server (Braintree, Stripe, etc) or they might be transmitted
 * directly through the server (PayPal Payments Pro).
 *
 * The OnsitePaymentGatewayInterface is the base interface which all onsite
 * gateways implement. The other interfaces signal which additional capabilities
 * the gateway has. The gateway plugin is free to expose additional methods,
 * which would be defined below.
 */
interface OnsiteInterface extends OnsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface {

   /**
    * Generates the card token.
    *
    * Used by the add-payment-method plugin form.
    *
    * @param \Drupal\commerce_order\Entity\OrderInterface $order
    *
    * @todo figure out how to inject the order into the add-payment-method plugin when an admin is creating a payment
    *   method for an order
    *
    * @return string
    *   The card token or null if there was an error
    */
    public function generateCardToken($order = null);

   /**
    *  Gets the percentage of the payment taken by the platform
    *
    * @return float
    */
    public function getGatewayPercentage();

   /**
    * Gets the gateway transaction fee, in cents
    *
    * @return integer
    */
    public function getGatewayTransactionFee();
}
