<?php

namespace Drupal\example_promisepay_integration;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Payout portion entity.
 *
 * @see \Drupal\example_promisepay_integration\Entity\PPPayoutPortion.
 */
class PPPayoutPortionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\example_promisepay_integration\Entity\PPPayoutPortionInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished payout portion entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published payout portion entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit payout portion entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete payout portion entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add payout portion entities');
  }

}
