<?php

namespace Drupal\example_promisepay_integration\Entity;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_promisepay\PromisePayAPI;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce\Context;

/**
 * Defines the Payout entity.
 *
 * @ingroup example_promisepay_integration
 *
 * @ContentEntityType(
 *   id = "payout",
 *   label = @Translation("Payout"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\example_promisepay_integration\PPPayoutListBuilder",
 *     "views_data" = "Drupal\example_promisepay_integration\Entity\PPPayoutViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\example_promisepay_integration\Form\PPPayoutForm",
 *       "add" = "Drupal\example_promisepay_integration\Form\PPPayoutForm",
 *       "edit" = "Drupal\example_promisepay_integration\Form\PPPayoutForm",
 *       "delete" = "Drupal\example_promisepay_integration\Form\PPPayoutDeleteForm",
 *     },
 *     "access" = "Drupal\example_promisepay_integration\PPPayoutAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\example_promisepay_integration\PPPayoutHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "payout",
 *   admin_permission = "administer payout entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/payout/{payout}",
 *     "add-form" = "/admin/structure/payout/add",
 *     "edit-form" = "/admin/structure/payout/{payout}/edit",
 *     "delete-form" = "/admin/structure/payout/{payout}/delete",
 *     "collection" = "/admin/structure/payout",
 *   },
 *   field_ui_base_route = "payout.settings"
 * )
 */
class PPPayout extends ContentEntityBase implements PPPayoutInterface {

  use EntityChangedTrait;

  /**
   * @var amount to be deducted from the platform fee
   *  not the same as the gateway_portion, which also includes the amount due the gateway from the seller
   */
  protected $gateway_fee_deduction;

  /**
   * @var seller id
   */
  public $sellerId;

  /**
   * {@inheritdoc}
   */
  public function getOrder() {
    return $this->get('order_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrder(OrderInterface $order) {
    $this->set('order_id', $order->id());
  }

  /**
   * {@inheritdoc}
   */
  public function getPayment() {
    return $this->get('payment_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setPayment(PaymentInterface $payment) {
    $this->set('payment_id', $payment->id());
  }

  /**
   * {@inheritdoc}
   */
  public function getPortions() {
    return $this->get('portions')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function addPortion($payee, $amount, OrderInterface $order, $item_id = '') {
    $portion = PPPayoutPortion::create([
      'user_id' => $payee,
      'order_id' => $order->id(),
      'amount' => $amount,
      'remote_item_id' => $item_id
    ]);
    $portion->save();
    $this->get('portions')->appendItem($portion);
  }

  /**
   * {@inheritdoc}
   */
  public function getPlatformPercent() {
    return $this->get('platform_percent')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlatformPercent($percent) {
    $this->set('platform_percent', $percent);
  }

  /**
   * {@inheritdoc}
   */
  public function getAffiliatePercent() {
    return $this->get('affiliate_percent')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAffiliatePercent($percent) {
    $this->set('affiliate_percent', $percent);
  }

  /**
   * {@inheritdoc}
   */
  public function getGatewayPercent() {
    return $this->get('gateway_percent')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGatewayPercent($percent) {
    $this->set('gateway_percent', $percent);
  }

  /**
   * {@inheritdoc}
   */
  public function getGatewayTransactionFee() {
    return $this->get('gateway_transaction_fee')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGatewayTransactionFee($fee) {
    $this->set('gateway_transaction_fee', $fee);
  }

  /**
   * {@inheritdoc}
   */
  public function getSellFeeId() {
    return $this->get('sell_fee')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSellFeeId($fee_id) {
    $this->set('sell_fee', $fee_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getSellerPortion() {
    return $this->get('seller_amount')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSellerPortion($amount) {
    $this->set('seller_amount', intval(round($amount)));
  }

  /**
   * {@inheritdoc}
   */
  public function getGatewayPortion() {
    return $this->get('gateway_amount')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGatewayPortion($amount) {
    $this->set('gateway_amount', intval(round($amount)));
  }

  /**
   * {@inheritdoc}
   */
  public function getPlatformNetPortion() {
    return $this->get('platform_net_portion')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlatformNetPortion($amount) {
    $this->set('platform_net_portion', intval(round($amount)));
  }

  /**
   * {@inheritdoc}
   */
  public function getTaxCollected() {
    return $this->get('tax_collected')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTaxCollected($amount) {
    $this->set('tax_collected', intval(round($amount)));
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isComplete() {
    return (bool) $this->getEntityKey('status');
  }
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setComplete($complete) {
    $this->set('status', $complete ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function calculatePayouts(OrderInterface $order, $amount) {
    // here you implement your custom business logic for making payouts to the relevant users
    // probably you want to store those user ids on the order some time prior to checkout
    // then calculate who gets what. When it comes time to release the payments,
    // the primary releasePayment will give the seller his funds and collect the platform fee
    // but if anyone else (e.g. an affiliate) needs to be payed, you need to handle that
    // based on what you are creating here at the time of checkout; the payout portions
    //
    //  $this->addPortion($portion_recipient_id, $portion_amount_cents, $order);

  }

  /**
   * {@inheritdoc}
   */
  public function createFee($payment_amount) {
    $seller_portion = $this->getSellerPortion();
    $fee_amount = $payment_amount - $seller_portion - $this->gateway_fee_deduction;
    // the item will pay the seller the total payment amount
    // less our fee amount, but will also subtract the gateway fee
    // so we must deduct the gateway fee amount from our fee.
    $values = [
      'amount'      => $fee_amount,
      'name'        => 'Apportionment fee',
      'fee_type_id' => '1',
      'to'          => 'seller'
    ];
    $response = PromisePayAPI::createFee($values);
    if(isset($response['error'])) {
      \Drupal::logger('promisepay_integration')->error('fee creation error: ' . $response['error']);
      throw new HardDeclineException(t('Payment could not be processed. Please contact support.'));
    } else {
      $this->setSellFeeId($response['id']);
      return $this->getSellFeeId();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Payout entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['platform_percent'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Platform percent'))
      ->setDefaultValue(20)
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['platform_net_portion'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Platform net portion of sale, after fees'))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['tax_collected'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Sales tax collected'))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['affiliate_percent'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Affiliate percent'))
      ->setDefaultValue(10)
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -3,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['gateway_transaction_fee'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Gateway transaction fee'))
      ->setDefaultValue(30)
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => -1,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['gateway_percent'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Gateway percent'))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => 0,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['sell_fee'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Apportionment Fee ID'))
      ->setDescription(t('Id of the fee applied to collect the non-seller portion of the funds'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 2,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['seller_amount'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Seller Amount'))
      ->setDescription(t('Seller portion in cents'))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => 4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['gateway_amount'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Gateway Amount'))
      ->setDescription(t('Gateway portion in cents'))
      ->setDisplayOptions('form', array(
        'type' => 'number',
        'weight' => 5,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['portions'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Other portions'))
      ->setDescription(t('All portions not belonging to the seller.'))
      ->setSetting('target_type', 'payout_portion')
      ->setSetting('handler', 'default')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', array(
        'type' => 'inline_entity_form_complex',
        'weight' => 6,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order'))
      ->setSetting('target_type', 'commerce_order')
      ->setSetting('handler', 'default');

    $fields['payment_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Payment'))
      ->setSetting('target_type', 'commerce_payment')
      ->setSetting('handler', 'default');

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Payout status'))
      ->setDescription(t('A boolean indicating whether the Payout is complete.'))
      ->setDefaultValue(FALSE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
