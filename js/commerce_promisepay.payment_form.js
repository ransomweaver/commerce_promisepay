/**
 * @file
 * Defines behaviors for the PromisePay payment method form.
 */
(function ($, Drupal, drupalSettings, promisepay) {

  'use strict';

  /**
   * Attaches the promisepay behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   */
  Drupal.behaviors.commercePromisePayPaymentForm = {
    attach: function (context) {

      var $multistep_form = $('[id^="commerce-checkout-flow-"]');
      window.formId = $multistep_form.first().attr('id');
      var form = document.getElementById(window.formId);
      // if there was an ajax reload, we remove our listeners
      if (form) {
        form.removeEventListener("submit", Drupal.paymentMethodSubmit);
        form.removeEventListener("submit", Drupal.paymentExistingSubmit);
        $('[id^="edit-actions-next"]').attr("disabled", "disabled");
      }
      var $main_form = $('.promisepay-card-form', context).closest('form');
      if ($main_form.length > 0) {

        $('.promisepay-card-form', context).each( function () {
          $('[data-numeric]', $main_form).payment('restrictNumeric');
          $('#cc-number', $main_form).payment('formatCardNumber');
          $('#cc-exp', $main_form).payment('formatCardExpiry');
          $('#cc-cvc', $main_form).payment('formatCardCVC');

          $('#cc-number', $main_form).keyup(function() {
            delay(function(){
              validateCard();
            }, 1000 );
          });

          $('#cc-exp', $main_form).keyup(function() {
            delay(function(){
              validateCard();
            }, 1000 );
          });

          $('#cc-cvc', $main_form).keyup(function() {
            delay(function(){
              validateCard();
            }, 1000 );
          });

          console.log('token: ' + drupalSettings.promisepay.token);

          if (!window.promisepayInitialized) {
            promisepay.configure(drupalSettings.promisepay.environment);
            promisepay.captureDeviceId(Drupal.deviceIdSuccess);
            promisepay.getIPAddress(Drupal.ipSuccess);
            // select the target node
            var error_p = document.getElementById('promisepay-server-error');

            // create an observer instance
            var observer = new MutationObserver(function(mutations) {
              mutations.forEach(function(mutation) {
                if ($('.has-error').length ) {
                  $('#cover').remove();
                  $('#spinner').remove();
                }
              });
            });

            // configuration of the observer:
            var config = { attributes: false, childList: true, characterData: true };

            // pass in the target node, as well as the observer options
            observer.observe(error_p, config);
          }

          $.fn.toggleInputError = function (erred) {
            this.parent('.form-item', $main_form).toggleClass('has-error', erred);
            return this;
          };
          form.addEventListener("submit", Drupal.paymentMethodSubmit);
        });
      } else {
        $('#' + window.formId).each( function () {
          form.addEventListener("submit", Drupal.paymentExistingSubmit);
         // $('[id^="edit-actions-next"]').removeAttr("disabled");
        });

      }
    },
    detach: function (context) {
      var form = document.getElementById(window.formId);
      if (form) {
        form.removeEventListener("submit", Drupal.paymentMethodSubmit);
        form.removeEventListener("submit", Drupal.paymentExistingSubmit);
      }
    }
  };


  Drupal.paymentExistingSubmit = function(e) {
    var $cover = $( "<div id='cover'></div>" );
    $('body').append($cover);
    var $spinner = $( "<div id='spinner'></div>" );
    $('body').append($spinner);
  };

  Drupal.paymentMethodSubmit = function(e) {
    e.preventDefault();
    var $cover = $( "<div id='cover'></div>" );
    $('body').append($cover);
    var $spinner = $( "<div id='spinner'></div>" );
    $('body').append($spinner);
    var $number = $('#cc-number');
    var $exp = $('#cc-exp');
    var cardNumber = $number.val();
    var cardType = $.payment.cardType(cardNumber);
    var $cvc = $('#cc-cvc');
    var cvv = $cvc.val();
    var expiryObj = $exp.payment('cardExpiryVal');

    var given_name = '';
    var family_name = '';
    var $reuse_shipping = $('[id^="edit-payment-information-add-payment-method-billing-information-copy-fields-enable"]');
    if ($reuse_shipping.length && $reuse_shipping.attr("checked")) {
      var $firstName = $('[id^="edit-shipping-information-shipping-profile-address-0-address-given-name"]');
      if ($firstName.length) {
        // value from input
        var $lastName = $('[id^="edit-shipping-information-shipping-profile-address-0-address-family-name"]');
        $firstName.toggleInputError($firstName.val() == '');
        $lastName.toggleInputError($lastName.val() == '');
        given_name = $firstName.val();
        family_name = $lastName.val();
      } else {
        // value from html
        given_name = $('[id^="edit-shipping-information-shipping-profile"] .given-name').text();
        family_name = $('[id^="edit-shipping-information-shipping-profile"] .family-name').text();
      }
    } else {
      // use billing for cc full_name
      var $firstBillingName = $('[id^="edit-payment-information-add-payment-method-billing-information-address-0-address-given-name"]');
      if ($firstBillingName.length) {
        // value from input
        var $lastBillingName = $('[id^="edit-payment-information-add-payment-method-billing-information-address-0-address-family-name"]');
        $firstBillingName.toggleInputError($firstBillingName.val() == '');
        $lastBillingName.toggleInputError($lastBillingName.val() == '');
        given_name = $firstBillingName.val();
        family_name = $lastBillingName.val();
      } else {
        // value from html
        given_name = $('[id^="edit-payment-information-add-payment-method-billing-information"] .given-name').text();
        family_name = $('[id^="edit-payment-information-add-payment-method-billing-information"] .family-name').text();
      }
    }

    $number.toggleInputError(!$.payment.validateCardNumber(cardNumber));
    $exp.toggleInputError(!$.payment.validateCardExpiry(expiryObj));
    $cvc.toggleInputError(!$.payment.validateCardCVC(cvv, cardType));

    var $error = $('.promisepay-server-error');
    $error.html('');
    if ($('.has-error').length ) {
      $error.html('<strong>Card validation errors</strong>').css('display','block');
    } else {
      $('#cc-type').val(cardType);
      $('#last-4').val(cardNumber.substr(cardNumber.length - 4));
      $('#expiry-month').val(expiryObj.month);
      $('#expiry-year').val(expiryObj.year);
      var fullName = given_name + ' ' + family_name;

      promisepay.createCardAccount(drupalSettings.promisepay.token, {
        full_name: fullName,
        number: cardNumber,
        expiry_month: expiryObj.month,
        expiry_year: expiryObj.year,
        cvv: cvv,
        currency: 'USD'
      }, Drupal.cardSuccess, Drupal.ppFail);
    }
  };

  var delay = (function(){
    var timer = 0;
    return function(callback, ms){
      clearTimeout (timer);
      timer = setTimeout(callback, ms);
    };
  })();

  function validateCard() {
    var ccvalid = $.payment.validateCardNumber($('#cc-number').val());
    var expvalid = $.payment.validateCardExpiry($('#cc-exp').payment('cardExpiryVal'));
    var cvcvalid = $.payment.validateCardCVC($('#cc-cvc').val());
    if (ccvalid && expvalid && cvcvalid) {
      $('[id^="edit-actions-next"]').removeAttr("disabled");
    } else {

      $('[id^="edit-actions-next"]').attr("disabled", "disabled");
    }
  }

  Drupal.deviceIdSuccess = function(data) {
    $('#device-id').val(data);
  };

  Drupal.ipSuccess = function(data) {
    $('#ip-address').val(data);
    window.promisepayInitialized = true;
  };

  Drupal.cardSuccess = function(data) {
    var id = data['id'];
    $('#pp-account-id').val(id);
    $('#cc-number').val('');
    $('#cc-exp').val('');
    $('#cc-cvc').val('');
    setTimeout(function () {
        $('#cover').remove();
        $('#spinner').remove();
    }, 5000);
    var form = document.getElementById(window.formId);
    form.submit();
  };


  Drupal.ppFail = function(data) {
    $('#cover').remove();
    $('#spinner').remove();
    if (typeof(data) === 'string') {
      var errors = $.parseJSON(data);
      var error_string = '<strong>Errors:</strong><br>';
      for (var i = 0, len = errors.length; i < len; i++) {
        var err = errors[i];
        error_string += err['string'] + '<br>';
      }
      $('.promisepay-server-error').html(error_string);
    } else {
      var status = data.statusText;
      if (status) {
        var response = data.responseText.replace(/"/g, '').replace(/\{|\}/g, ' ').replace(/\[|\]/g, ' ');
        if (response.includes('token')) {
          response += '<br>Reload the form';
        }
        $('.promisepay-server-error').html(status + ': ' + response).css('display','block');
      }
    }
  };


})(jQuery, Drupal, drupalSettings, window.promisepay);

