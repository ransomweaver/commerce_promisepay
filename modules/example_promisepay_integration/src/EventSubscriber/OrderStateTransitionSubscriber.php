<?php

namespace Drupal\example_promisepay_integration\EventSubscriber;

use Drupal\commerce_order\OrderTotalSummaryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\example_promisepay_integration\PPHelper;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_promisepay\PromisePayAPI;
use Drupal\commerce_payment\Entity\PaymentInterface;


class OrderStateTransitionSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The order type entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderTypeStorage;

  /**
   * The order total summary.
   *
   * @var \Drupal\commerce_order\OrderTotalSummaryInterface
   */
  protected $orderTotalSummary;


  /**
   * Constructs a new OrderStateTransitionSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_order\OrderTotalSummaryInterface $order_total_summary
   *   The order total summary.
   *   The renderer.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, OrderTotalSummaryInterface $order_total_summary) {
    $this->orderTypeStorage = $entity_type_manager->getStorage('commerce_order_type');
    $this->orderTotalSummary = $order_total_summary;
  }

  /**
   * Cancel the item on PP and refund amount to buyer.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event we subscribed to.
   */
  public function cancelItemOnPP(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $payouts = $order->get('payouts')->referencedEntities();
    if ($payouts) {
      foreach ($payouts as $payout) {
        $payment = $payout->getPayment();
        $refunded = FALSE;
        if ($payment) {
          $refunded = $this->refundPayment($payment, t('Order cancelled'));
        }
        if (!$refunded) {
          $event->stopPropagation();
          drupal_set_message(t('Order could not be refunded on PromisePay'), 'error');
        }
      }
    }
  }

  /**
   * Refund Order amount to buyer.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event we subscribed to.
   */
  public function returnedRefundPaymentOnPP(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();

    $refunded = false;
    $payouts = $order->get('payouts')->referencedEntities();
    if ($payouts) {
      foreach ($payouts as $payout) {
        $payment = $payout->getPayment();
        if ($payment) {
          $refunded = $this->refundPayment($payment, t('Order returned'));
        }
      }
      if (!$refunded) {
        $event->stopPropagation();
        drupal_set_message(t('Order could not be refunded on PromisePay'), 'error');
      }
    }
  }

  /**
   * Release the order funds to the sellers.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event we subscribed to.
   */
  public function completedReleasePaymentOnPP(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $released = false;
    $payouts = $order->get('payouts')->referencedEntities();
    if ($payouts) {
      foreach ($payouts as $payout) {
        $payment = $payout->getPayment();
        if ($payment) {
          $payment_state = $payment->state;
          if ($payment_state == 'capture_refunded') {
            $event->stopPropagation();
            drupal_set_message(t('Order already fully refunded'), 'error');
            return;
          }
          else {
            // TODO: are there other payment states that prevent releasing the balance?
            $releaseResponse = PromisePayAPI::releasePayment($payment->uuid());
            if (isset($releaseResponse['error'])) {
              \Drupal::logger('promisepay_integration')->error('payout not processed: ' . $releaseResponse['error']);
            } else {
              $released = TRUE;
              $payout->setComplete(true);
              $payout->save();
              PPHelper::releasePayouts($payout);
            }
          }
        }
      }
    }
    if (!$released) {
      $event->stopPropagation();
      drupal_set_message(t('Order payment could not be released on PromisePay'), 'error');
    }
  }

  private function refundPayment(PaymentInterface $payment, $message) {
    $payment_state = $payment->state;
    if ($payment_state == 'capture_refunded') {
      drupal_set_message(t('Order already fully refunded'));
      return true;
    }
    // unlike the refund in Drupal\commerce_promisepay\Plugin\Commerce\PaymentGateway\Onsite,
    // this refund is a full refund of the balance.
    $amount = $payment->getBalance();
    $amount_cents = intval(floatval($amount->getNumber()) * 100);
    $refundResponse = PromisePayAPI::refund($payment->uuid(), $amount_cents, $message);
    if (isset($refundResponse['error'])) {
      return false;
    }
    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    $payment->state = 'capture_refunded';

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();

    return true;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.cancel.pre_transition' => ['cancelItemOnPP', -100],
      'commerce_order.complete.pre_transition' => ['completedReleasePaymentOnPP', -100],
      'commerce_order.return_is_ok.pre_transition' => ['returnedRefundPaymentOnPP', -100],
    ];
    return $events;
  }

}
