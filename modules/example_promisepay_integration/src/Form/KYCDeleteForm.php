<?php

namespace Drupal\example_promisepay_integration\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Know Your Customer entities.
 *
 * @ingroup example_promisepay_integration
 */
class KYCDeleteForm extends ContentEntityDeleteForm {


}
