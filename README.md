Drupal Commerce PromisePay
===============

PromisePay (http://https://promisepay.com) is a payments backend for marketplaces and platforms.

This is not a turnkey payments solution. It requires implementation of an integration module.

This module consists of two parts:

1) commerce_promisepay module provides a Drupal Commmerce payment gateway plugin to collect a credit card payment method, and wraps the PromisePay php API and promisepay.js in Drupalness.

2) example_promisepay_integration module shows how one might implement a custom business logic to take payment on an order (via a hook provided by commerce_promisepay) then later release payment to the primary seller and any other "portions" requiring disbursement of funds from the sale.

Contact ransom@ransomweaver.com with questions, or @ransomweaver on #drupal-commerce
