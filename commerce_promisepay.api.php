<?php


/**
 * @file
 * Hooks provided by the Commerce PromisePay module.
 */

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_promisepay\Plugin\Commerce\PaymentGateway\OnsiteInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Act on a payment in a custom process
 *
 * @param PaymentInterface $payment
 * @param PaymentMethodInterface $payment_method
 * @param boolean $capture
 * @param OnsiteInterface $plugin
 * @return bool
 */
function hook_promisepay_process_payment(PaymentInterface &$payment, PaymentMethodInterface $payment_method, $capture, OnsiteInterface $plugin) {
  $amount = $payment->getAmount();
  $amount_cents = intval($amount->getNumber()) * 100;
  $payment_method_id = $payment_method->getRemoteId();


  $payment_type_id = 1;  // 1 = Escrow, 2 = Express, 3 = Escrow Partial Release, 4 = Approve
  // create value array for PromisePay createItem call
  $order = $payment->getOrder();
  $values = [
    'id' => $payment->uuid(),
    'name' => $order->getStore()->getName() . ' Order #' . $order->getOrderNumber(),
    'amount' => $amount_cents,
    'payment_type' => $payment_type_id,
    'fee_ids' => 'FEE_ID',
    'buyer_id' => $order->getCustomer()->uuid(),
    'seller_id' => $order->getStore()->getOwner()->uuid(),
    'description' => '',
    'custom_descriptor' => $order->getStore()->getName()
  ];
  // create item
 /* $itemResponse = PromisePayAPI::createItem($values);
  if ($response['error']) {
    drupal_set_message($response['error'], 'error');
    return false;
  } */

  //$payment->setRemoteState($itemResponse['state']);
  //$payment->state = 'authorization';
  $payment->setAuthorizedTime(REQUEST_TIME);

  // $capture for PromisePay means makePayment on the Item to the seller at the time of order completion
  // for now it will immediately charge the user's payment method (card) and put the money into escrow
  // later (after the return period) we need to release the payment (which pays the seller and collects the fee)
 /* if ($capture) {
    $paymentResponse = PromisePayAPI::makePayment(
      $payment->uuid(),
      $payment_method_id,
      $payment_method->get('ip_address')->value,
      $payment_method->get('device_id')->value
    );
    if ($paymentResponse['error']) {
      drupal_set_message($paymentResponse['error'], 'error');
      throw new HardDeclineException(t('The payment method could not be processed. Try another card or contact support.'));
    } else {
      $payment->setRemoteState($paymentResponse['state']);
      $payment->state = 'capture_completed';
      $payment->setCapturedTime(REQUEST_TIME);
    }
  }
 */
  $payment->setTest($plugin->getMode() == 'test');
  //$payment->setRemoteId($itemResponse['id']);
  $payment->setRemoteId(123);
  $payment->save();

  return true;
}

function hook_promisepay_process_refund(PaymentInterface &$payment, $item_id, $amount, $refund_message = NULL) {
  $order = $payment->getOrder();
  $refund_message = $order->getData('refund_message'); // optional refund message from your UI. could be null

  /*
    $refundResponse = PromisePayAPI::refund(
      $item_id,
      $amount,
      $refund_message
    );
    if ($refundResponse['error']) {
      drupal_set_message($refundResponse['error'], 'error');
      return false;
    } else {
      // your custom logic
    }

 */

  return true;
}

/**
 * @} End of "addtogroup hooks".
 */
