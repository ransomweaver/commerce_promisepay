<?php

namespace Drupal\example_promisepay_integration\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Payout portion edit forms.
 *
 * @ingroup example_promisepay_integration
 */
class PPPayoutPortionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\example_promisepay_integration\Entity\PPPayoutPortion */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Payout portion.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Payout portion.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.payout_portion.canonical', ['payout_portion' => $entity->id()]);
  }

}
